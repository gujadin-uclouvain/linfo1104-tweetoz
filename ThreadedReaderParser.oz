functor
import
    Reader
export
    producerReaderParser:ProducerReaderParser
    split:Split

define
    proc {Split Sentence ?R}
    % Split a sentence (FOR THE PARSER (BACKEND) IMPLEMENTATION)
        fun {F String List MotAdditionner}
            fun {HeadOfTail L} 
                case L 
                of nil then true
                []H|_ then {Char.isSpace H} end 
            end
        in
            case String
            of nil then 
                if MotAdditionner == nil then List
                else {Append List [MotAdditionner]}
                end
            [] H|T then
                case {Char.type H}
                of lower then {F T List {Append MotAdditionner [H]}} % cas de base
                [] upper then {F T List {Append MotAdditionner [{Char.toLower H}]}} % char is lower
                [] digit then {F T List {Append MotAdditionner [H]}}
                [] punct then
                    if H == 44 then % ","
                        if MotAdditionner == nil then {F T List nil}
                        else
                            {F T {Append List [MotAdditionner]} nil}
                        end
                    elseif H == 39 then {F T List {Append MotAdditionner [H]}} % "'"
                    elseif H == 64 then {F T {Append List ["@"]} nil} % "@"
                    elseif H == 35 then {F T {Append List ["#"]} nil} % "#"
                    elseif H == 45 andthen {Not {HeadOfTail T}} then {F T List {Append MotAdditionner [H]}} % keep "-" if there is a non-space after
                    elseif H == 38 then {F T.2.2.2.2 {Append List ["&"]} nil} % "&amp;" => "&"
                    else
                        if MotAdditionner == nil then {F T {Append List [nil]} nil}
                        else
                            {F T {Append {Append List [MotAdditionner]} [nil]} nil}
                        end
                    end
                else
                    if MotAdditionner == nil then {F T List nil}
                    elseif H == 8217 then {F T List {Append MotAdditionner [39]}} % "’" => "'"
                    else
                        {F T {Append List [MotAdditionner]} nil}
                    end
                end
            end
        end
    in
        R = {F Sentence nil nil}
    end

    proc {ProducerReaderParser P NbrThreads NbrFiles}
    % Active the multi-threaded reading-parsing
    % @pre: - NbrThreads: the number of thread for the reading (x2 with the parsing)
    %       - NbrFiles: the number of files
        proc {ThreadsCreator P NbrThreads CurrThread NbrFiles}
            proc {ProducerReaderNFiles CurrFile MaxFile ?R}
                fun {HowManyLines File_N} if File_N == 208 then 72 else 100 end end %208 = file with 72 lines
                fun {ProducerReaderNFilesAcc CurrFile MaxFile CurrLine MaxLine}
                    if CurrFile > MaxFile then nil
                    elseif CurrLine > MaxLine then {ProducerReaderNFilesAcc CurrFile+1 MaxFile 1 {HowManyLines CurrFile+1}}
                    else {Reader.getNLine CurrFile CurrLine}|{ProducerReaderNFilesAcc CurrFile MaxFile CurrLine+1 MaxLine}
                    end
                end
            in
                R = {ProducerReaderNFilesAcc CurrFile MaxFile 1 {HowManyLines CurrFile+1}}
            end
            proc {ProducerParser P S}
                case S of nil then {Send P done}
                [] H|T then {Send P {Split H}} {ProducerParser P T}
                end
            end
            S 
        in
            if CurrThread =< NbrThreads then
                thread
                    S = {ProducerReaderNFiles
                        ((NbrFiles div NbrThreads)*(CurrThread-1))+1 
                        ((NbrFiles div NbrThreads)*CurrThread)
                    } 
                end
                thread {ProducerParser P S} end
                {ThreadsCreator P NbrThreads CurrThread+1 NbrFiles}
            end
        end
    in
        {ThreadsCreator P NbrThreads 1 NbrFiles}
    end
end 