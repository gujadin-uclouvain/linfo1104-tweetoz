# Twit’Oz - Groupe AR

* Gaudin Félix
* Jadin Guillaume

## Files
`main.oz` -> va s'occuper de tout lancer / afficher l'interface

`DictionaryExtractor.oz` -> Fonctions qui permettent de récupérer une valeur à partir d'un dictionnaire

`DictionaryManager.oz` -> Fonctions pour ajouter des entrées au dictionnaire

`GUIManager.oz` -> Fonctions de manipulation des strings envoyé par l'interface

`MakeFile` -> Le make file (voir à la section du makefile)
 
`Reader.oz` -> Fonctions pour lire les fichiers

`ThreadedReaderParser.oz ` -> Fonctions qui vont gérer le multithreading 

## MakeFile
`$ make all` -> compile le programme

`$ make run` -> compile et lance le programme
> On affiche des informations dans le terminal penant que le programme charge les données

`$ make clean` -> supprime les fichiers compilés (.oza/.ozf)

## Raccourcis claviers
Dans l'interface graphique vous pouvez utiliser des raccourcis claviers :
* Ctrl + Q -> activer/désactiver l'auto-fetch
* Ctrl + S -> fetch
* Ctrl + W -> Choisir le premier mot
* Ctrl + X -> Choisir le deuxième mot
* Ctrl + C -> Choisir le troisième mot

## GitHub
Le lien de notre [GitHub](https://github.com/FelixGaudin/TweetOz).

> Si vous rencontrez un soucis, veuillez nous le dire par [mail (Gaudin F.)](felix.gaudin@student.uclouvain.be) ou [mail (Jadin G.)](guillaume.l.jadin@student.uclouvain.be)