functor
export
    strip:Strip
    getLastWord:GetLastWord

define
    proc {Split Sentence ?R}
    % Split a sentence (FOR THE GUI IMPLEMENTATION)
        fun {F String List MotAdditionner}
            case String
            of nil then 
                if MotAdditionner == nil then List
                else {Append List [MotAdditionner]}
                end
            [] H|T then
                case {Char.type H}
                of lower then {F T List {Append MotAdditionner [H]}}
                [] upper then {F T List {Append MotAdditionner [H]}}
                [] digit then {F T List {Append MotAdditionner [H]}}
                [] punct then
                    if MotAdditionner == nil then
                        {F T {Append List [[H]]} nil}
                    else
                        {F T {Append {Append List [MotAdditionner]} [[H]]} nil}
                    end
                else
                    if MotAdditionner == nil then {F T List nil}
                    elseif H == 8217 then {F T List {Append MotAdditionner [39]}} % "’" => "'"
                    else
                        {F T {Append List [MotAdditionner]} nil}
                    end
                end
            end
        end
    in
        R = {F Sentence nil nil}
    end

    proc {Strip L ?R}
    % Strip the user's sentences
        fun {Flater L}
            if L == nil then L
            elseif L.2 == nil then L.1
            else 
                if {Char.isPunct L.2.1.1} then
                    case L.2.1.1
                    of 35 then {Append {Append L.1 " "} {Flater L.2}} % "#"
                    [] 38 then {Append {Append L.1 " "} {Flater L.2}} % "&"
                    [] 64 then {Append {Append L.1 " "} {Flater L.2}} % "@"
                    else {Append L.1 {Flater L.2}} end
                else {Append {Append L.1 " "} {Flater L.2}}
                end
            end
        end
    in
        R = {Flater {Split L}}
    end
    
    proc {GetLastWord L ?R}
    % Get the last word of the user's sentences
        fun {GetLast L}
            if L == nil then nil
            elseif {Char.isPunct L.1.1} then 
                case L.1.1
                    of 33 then [33] % "!"
                    [] 35 then [35] % "#"
                    [] 63 then [63] % "?"
                    [] 64 then [64] % "@"
                    [] 46 then [46] % "."
                    [] 38 then [38] % "&"
                    else
                        {GetLast L.2}
                end
            else L.1
            end
        end 
    in
        R = {Map {GetLast {List.reverse {Split L}}} Char.toLower}
    end
end
