functor
import
    ThreadedReaderParser
    System
export
    init:DictionaryGenerator

define
    fun {AddSentenceInDict Dict List}
    % Save a parsed sentence (List) in the dictionnary (Dict)
        proc {AddInDict Dict Word NextWord ?R}
            fun {StrToAtom Word} if {Atom.is Word} then Word else {String.toAtom Word} end end
            fun {P Dict Word NextWord}
                if Word == nil orelse NextWord == nil then Dict
                elseif {Not {Dictionary.member Dict Word}} then
                    {Dictionary.put Dict Word {Dictionary.new}} % créer un dictionnaire à la key Word
                    {AddInDict Dict Word NextWord}
                else
                    {Dictionary.put {Dictionary.get Dict Word} NextWord {Dictionary.condGet {Dictionary.get Dict Word} NextWord 0}+1}
                    Dict
                end
            end
        in
            R = {P Dict {StrToAtom Word} {StrToAtom NextWord}}
        end
    in
        case List
        of Word|T then
            case T of nil then Dict
            [] NextWord|_ then {AddSentenceInDict {AddInDict Dict Word NextWord} T}
            end
        end
    end

    proc {ConsumerRecording S NbrThreads ?R}
    % Browse to the stream S and call the AddSentenceInDict function
    % NB: done appear when a thread has finished read these assigned files
        fun {ConsumerRecordingAcc S Dict NbrThreads NbrFinishedThreads}
            case S
            of H|T then
                if H == done then
                    if NbrFinishedThreads == (NbrThreads div 2) then {System.showInfo '%**'} {System.showInfo '%** the process is almost done...'} end
                    if NbrFinishedThreads == NbrThreads then Dict
                    else {ConsumerRecordingAcc T Dict NbrThreads NbrFinishedThreads+1} end
                else {ConsumerRecordingAcc T {AddSentenceInDict Dict H} NbrThreads NbrFinishedThreads} end
            end
        end
    in
        R = {ConsumerRecordingAcc S {Dictionary.new} NbrThreads-1 0}
    end
    
    fun {DictionaryGenerator}
    % Run all the multi-threaded functions to read, parse and save data
        proc {DictionaryGeneratorVar NbrThreads NbrFiles ?Dict} S P in
            {NewPort S P}
            {System.showInfo '%** reading and parsing files...'}
            {ThreadedReaderParser.producerReaderParser P NbrThreads NbrFiles}
            {System.showInfo '%** saving data...'}
            Dict = {ConsumerRecording S NbrThreads}
        end
    in
        {DictionaryGeneratorVar 8 208}
    end
end
