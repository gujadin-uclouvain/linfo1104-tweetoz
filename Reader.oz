functor
import
    Open
export
    getNLine:GetNLine

define
    fun {Scan InFile N}
    % Fetches the N-th line in a file
    % @pre: - InFile: a TextFile from the file
    %       - N: the desires Nth line
    % @post: Returns the N-th line or 'none' in case it doesn't exist
        Line={InFile getS($)}
    in
        if Line==false then
            {InFile close}
            none
        else
            if N==1 then
                {InFile close}
                Line
            else
                {Scan InFile N-1}
            end
        end
    end

    class TextFile 
    % This class enables line-by-line reading
        from Open.file Open.text
    end

    fun {GetNLine IN_NAME Line_N}
    % Clean function of Scan
        {Scan {New TextFile init(name:"tweets/part_"#IN_NAME#".txt")} Line_N}
    end
end