functor
import
    QTk at 'x-oz://system/wp/QTk.ozf'
    System
    Application
    DictionaryManager
    DictionaryExtractor
    GUIManager
define
%%% Easier macros for imported functions
    Show = System.showInfo

%%% Show informations on terminal #1
    {Show ''}
    {Show '%************************ Starting Twit’Oz **********************'}
    {Show '%**'}
    {Show '%** the process may takes +/- 1 minute'}
    {Show '%**'}

%%% Frontend
    Data = {DictionaryManager.init}

%%% Show informations on terminal #2
    {Show '% -------------------- completed'}
    {Show ''}

%%% GUI
    %% Make the window description
    Text1 Raw1 Raw2 Raw3 AutoFetchBox
    Description = td(
        title: "Twit'Oz: Make Oz Great Again"
        text(handle:Text1 font:TextFont width:75 height:15 bg:black foreground:white wrap:word glue:nwes tdscrollbar:true)
        lr(
            button(text:"Fetch" action:ActionFetchBtn height:2 glue:we)
            text(handle:Raw1 width:18 height:2 bg:white foreground:black glue:swe)
            button(text:">" action:ActionGetWord1 width:2 height:2 glue:we)
            text(handle:Raw2 width:18 height:2 bg:white foreground:black glue:swe)
            button(text:">" action:ActionGetWord2 width:2 height:2 glue:we)
            text(handle:Raw3 width:18 height:2 bg:white foreground:black glue:swe)
            button(text:">" action:ActionGetWord3 width:2 height:2 glue:we)
        )
        lr()
        tbcheckbutton(init:false action:ActionFetchBtn handle:AutoFetchBox text:"Auto Fetch" foreground:white)
        action:proc{$}{Application.exit 0} end % quit app gracefully on window closing
    )

    %% Button Actions
    proc {ActionFetchBtn} Max in
    % Action for the Fetch Button
        Max = {DictionaryExtractor.getMaxDict3 Data {GUIManager.getLastWord {Text1 getText(p(1 0) 'end' $)}}}
        {Raw1 set(Max.1.1)}
        {Raw2 set(Max.2.1)}
        {Raw3 set(Max.3.1)}
    end

    proc {ActionGetWord1}
    % Action to get the first word
        {Text1 set(
            {Append 
                {Append {GUIManager.strip {Text1 getText(p(1 0) 'end' $)}} " "} 
                {GUIManager.getLastWord {Raw1 getText(p(1 0) 'end' $)}}
            })
        }
        if {AutoFetchBox get($)} then {ActionFetchBtn} end
    end

    proc {ActionGetWord2}
    % Action to get the second word
        {Text1 set(
            {Append 
                {Append {GUIManager.strip {Text1 getText(p(1 0) 'end' $)}} " "} 
                {GUIManager.getLastWord {Raw2 getText(p(1 0) 'end' $)}}
            })
        }
        if {AutoFetchBox get($)} then {ActionFetchBtn} end
    end

    proc {ActionGetWord3}
    % Action to get the third word
        {Text1 set(
            {Append 
                {Append {GUIManager.strip {Text1 getText(p(1 0) 'end' $)}} " "} 
                {GUIManager.getLastWord {Raw3 getText(p(1 0) 'end' $)}}
            })
        }
        if {AutoFetchBox get($)} then {ActionFetchBtn} end
    end

    proc {ActionAutoFetchBox}
    % Action to activate/desactivate the Auto Fetch box
        if {AutoFetchBox get($)} == false then {ActionFetchBtn} end
        {AutoFetchBox set({Not{AutoFetchBox get($)}})}
    end

    %% Font
    TextFont = {QTk.newFont font(size:15)}

    %% Build the layout from the description
    Window={QTk.build Description}
    {Window show}

    %% Hotkeys
    {Text1 bind(event:"<Control-s>" action:ActionFetchBtn)}
    {Text1 bind(event:"<Control-w>" action:ActionGetWord1)}
    {Text1 bind(event:"<Control-x>" action:ActionGetWord2)}
    {Text1 bind(event:"<Control-c>" action:ActionGetWord3)}
    {Text1 bind(event:"<Control-q>" action:ActionAutoFetchBox)}
end
