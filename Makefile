all : main.oza
main.oza : main.oz Reader.ozf ThreadedReaderParser.ozf DictionaryManager.ozf DictionaryExtractor.ozf GUIManager.ozf
	ozc -c main.oz -o main.oza
%.ozf : %.oz
	ozc -c $< -o $@
run : main.oza
	ozengine main.oza
clean :
	rm -f *.oza *.ozf