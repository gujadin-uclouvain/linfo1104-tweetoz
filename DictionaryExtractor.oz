functor
export
    getMaxDict3:GetMaxDict3

define
    proc {GetMaxDict3 Dict Word ?R}
    % Get the 3 most recurring words after Word in Dict
    % @pre: - Dict: a Dictionary with sub-dictionaries
    %       - Word: the word to compare
    % @post: Returns a tuple with the 3 words and these numbers
    %           Exemple: max(make#10 america#8 great#4)
    %        If the word does not exist in Dict, return max(nil#~1 nil#~1 nil#~1)
        fun {StrToAtom Word} if {Atom.is Word} then Word else {String.toAtom Word} end end
        fun {F Dict KeyList KeyCurrentMax3}
            case KeyList
            of nil then KeyCurrentMax3
            [] Key|Tail then
                case KeyCurrentMax3 of max(V1 V2 V3) then
                    if {Dictionary.get Dict Key} > V1.2 then
                        {F Dict Tail max(Key#{Dictionary.get Dict Key} V1 V2)}
                    elseif {Dictionary.get Dict Key} > V2.2 then
                        {F Dict Tail max(V1 Key#{Dictionary.get Dict Key} V2)}
                    elseif {Dictionary.get Dict Key} > V3.2 then
                        {F Dict Tail max(V1 V2 Key#{Dictionary.get Dict Key})}
                    else
                        {F Dict Tail KeyCurrentMax3}
                    end
                end
            end
        end
    in
        if {Not {Dictionary.member Dict {StrToAtom Word}}} then R = max(nil#~1 nil#~1 nil#~1)
        else R = {F {Dictionary.get Dict {StrToAtom Word}} 
                    {Dictionary.keys {Dictionary.get Dict {StrToAtom Word}}}
                    max(nil#0 nil#0 nil#0)
                 }
        end
    end
end